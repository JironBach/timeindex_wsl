# -*- coding: utf-8 -*-
import csv
import re
import sys
argv = sys.argv

with open(argv[1], encoding='utf8') as f_in:
    with open('shiftjis/' + re.sub('results/', '', argv[1]), 'w', encoding='cp932') as f_out:
        f_out.write(f_in.read())

