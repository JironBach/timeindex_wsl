# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

'''
class NetkeibaItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
'''

class NetkeibaItem(scrapy.Item):
    枠 = scrapy.Field() #枠番
    馬番 = scrapy.Field() #馬番
    印 = scrapy.Field()
    馬名 = scrapy.Field() #馬名
    性齢 = scrapy.Field() #性齢
    斤量 = scrapy.Field() #斤量
    騎手 = scrapy.Field() #騎手
    最高 = scrapy.Field() #最高
    五走平均 = scrapy.Field() #５走平均
    距離 = scrapy.Field() #距離
    コース = scrapy.Field() #コース
    #recently_average = scrapy.Field() #近走平均
    三走 = scrapy.Field() #３走
    二走 = scrapy.Field() #２走
    前走 = scrapy.Field() #前走
    単勝オッズ = scrapy.Field() #オッズ
    人気 = scrapy.Field() #人気
