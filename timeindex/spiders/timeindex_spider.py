# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from timeindex.items import NetkeibaItem
import re

class TimeindexSpiderSpider(scrapy.Spider):
    name = 'timeindex_spider'
    #allowed_domains = ['race.netkeiba.com']
    #start_urls = ['https://race.netkeiba.com/race/speed.html?race_id=202009030811&rf=shutuba_submenu&fbclid=IwAR2evw_DY08EQ9ly-2gOegzbA_958PoJYv0C0TT-OKwqtt-o98TRh7Cvd74']
    allowed_domains = ['regist.netkeiba.com', 'race.netkeiba.com']
    start_urls = ['https://regist.netkeiba.com/account/?pid=login']

    def parse(self, response):
        try:
            print("login_id = " + str(self.id))
            print("password = " + str(self.pswd))
            return scrapy.FormRequest.from_response(
                response,
                formdata={'login_id': str(self.id), 'pswd': str(self.pswd)},
                callback=self.redirect_to_login, dont_filter=True,
                meta={
                    'handle_httpstatus_list': [302],
                },
            )
        except:
            return scrapy.FormRequest.from_response(
                response,
                formdata={'login_id': 'junshimo2@gmail.com', 'pswd': 'pa2010'},
                callback=self.redirect_to_login, dont_filter=True,
                meta={
                    'handle_httpstatus_list': [302],
                },
        )

    def redirect_to_login(self, response):
        try:
            print('crawl url = ' + str(self.url))
            yield scrapy.Request(url=self.url, callback=self.parse_main)
        except:
            yield scrapy.Request(url="https://race.netkeiba.com/race/speed.html?race_id=202009030811&rf=shutuba_submenu&fbclid=IwAR2evw_DY08EQ9ly-2gOegzbA_958PoJYv0C0TT-OKwqtt-o98TRh7Cvd74", callback=self.parse_main)


    def parse_main(self, response):
        p = re.compile(r"<[^>]*?>")
        table = response.xpath('//*[@class="SpeedIndex_Table Default RaceTable01 ShutubaTable"]')
        #print('table = ' + str(table))
        for row in table: #[1:]:
            th_rows = row.xpath('//tr').extract()
            print('')
            print('th_rows = '+str(th_rows))
            print('')
            '''
            item = NetkeibaItem()
            for th in th_rows:
                #self.print_td(td_rows)
                th_lines = p.sub("", th.strip()).splitlines()
                print('th_lines = ' + str(th_lines))
                if len(th_lines) > 20:
                    item['waku'] = th_lines[1] #枠番
                    item['umaban'] = th_lines[2] #馬番
                    item['checkmark'] = th_lines[5]
                    item['horse_name'] = th_lines[8] #馬名
                    item['horse_age'] = th_lines[9] #性齢
                    item['jockey_weight'] = th_lines[10] #斤量
                    item['jockey_name'] = th_lines[11] #騎手
                    item['best'] = th_lines[12] #最高
                    item['five_average'] = th_lines[13] #５走平均
                    item['length'] = th_lines[14] #距離
                    item['course'] = th_lines[15] #コース
                    #item['recently_average'] = th_lines[11] #近走平均
                    item['third_run'] = th_lines[16] #３走
                    item['second_run'] = th_lines[17] #２走
                    item['last_run'] = th_lines[18] #前走
                    item['odds'] = th_lines[19] #オッズ
                    item['popularity'] = th_lines[20] #人気

                    yield item
            '''

            td_rows = row.xpath('//tr').extract()
            print('')
            print('td_rows = '+str(td_rows))
            print('')

            item = NetkeibaItem()
            line_zero = True
            for td in td_rows:
                if line_zero:
                    line_zero = False
                    continue
                #self.print_td(td_rows)
                td_lines = p.sub("", td.strip()).splitlines()
                print('td_lines = ' + str(td_lines))
                if len(td_lines) > 20:
                    item['枠'] = td_lines[1] #枠番
                    item['馬番'] = td_lines[2] #馬番
                    item['印'] = td_lines[6]
                    item['馬名'] = td_lines[9] #馬名
                    item['性齢'] = td_lines[10] #性齢
                    item['斤量'] = td_lines[11] #斤量
                    item['騎手'] = td_lines[12] #騎手
                    item['最高'] = td_lines[13] #最高
                    item['五走平均'] = td_lines[14] #５走平均
                    item['距離'] = td_lines[15] #距離
                    item['コース'] = td_lines[16] #コース
                    #item['recently_average'] = td_lines[11] #近走平均
                    item['三走'] = td_lines[17] #３走
                    item['二走'] = td_lines[18] #２走
                    item['前走'] = td_lines[19] #前走
                    item['単勝オッズ'] = td_lines[20] #オッズ
                    item['人気'] = td_lines[21] #人気

                    yield item
        return
