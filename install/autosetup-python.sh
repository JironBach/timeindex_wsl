# build essential
sudo dpkg --configure -a
sudo apt update
sudo apt upgrade
sudo apt install -y build-essential libffi-dev libssl-dev zlib1g-dev liblzma-dev libbz2-dev libreadline-dev libsqlite3-dev git

# python setup
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
export PATH="~/.pyenv/bin:$PATH"
eval "$(pyenv init - zsh --no-rehash)"
eval "$(pyenv virtualenv-init -)"
sudo apt-get -y install bzip2 sqlite3
echo eval "$(pyenv init - zsh --no-rehash)" >> ~/.bashrc
echo eval "$(pyenv virtualenv-init -)" >> ~/.bashrc
pyenv install 3.8.5
sudo apt --fix-broken install -y

# scrapy setup
sudo apt-get -y install python3-dev python3-pip libxml2-dev libxslt1-dev
python3 -m pip install --upgrade pip
sudo ln -s /usr/bin/pip3 /usr/bin/pip
pip3 install scrapy
